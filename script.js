const fee = 0.2;

function removeFee(amount) {
  return (amount - amount * fee).toFixed(2);
}

function processAmount(amount) {
  return "$" + removeFee(amount.replace("$", ""));
}

const reportsHeader = document.querySelector(
  "#layout > div.layout-page-content > div > div:nth-child(2) > div.d-none.d-md-block > div > ul"
);

function getReportNav(index) {
  return reportsHeader.querySelector(
    `:scope > li:nth-child(${index}) > a > eo-tab-heading > tab-heading > h1 > strong`
  );
}

const inProgressNum = getReportNav(1);
inProgressNum.innerText = processAmount(inProgressNum.innerText);

const inReviewNum = getReportNav(2);
inReviewNum.innerText = processAmount(inReviewNum.innerText);

