# Upwork Reports Fee Remover

Removes Upwork's fees from the numbers shown on the [report page](https://www.upwork.com/ab/reports/in-progress).

Before:

![Before](./docs/before.png)

After:

![After](./docs/after.png)
